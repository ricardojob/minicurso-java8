/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.java8;

import edu.ifpb.java8.atividade.Pessoa;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Ricardo Job
 */
public class NewClass {

    public static void main(String[] args) {
        try {
            BufferedReader buffer = new BufferedReader(new FileReader("E:/build.xml"));
            List<String> lista = buffer.lines().collect(Collectors.toList());
            
            lista.forEach(a -> System.out.println(a));

        } catch (FileNotFoundException ex) {
            Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
