/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.java8;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author Ricardo Job
 */
public class LambdaInicio {

    public static void main(String[] args) {
        JButton botao = new JButton();

        botao.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Olá!");
            }
        });

        botao.addActionListener(action -> System.out.println(action.getSource()));
        botao.addActionListener(action -> System.out.println(action.toString()));
        
        
        
        
        

        botao.addActionListener(action -> tratarEvento(action));

    }

    public static void tratarEvento(ActionEvent e) {
         
    }

    @FunctionalInterface
    public interface OperadorDouble {

        public Double aplicar(Double a, Double b);

    }

    public class Pessoa {

        private String nome;
        private Integer idade;
        private String sexo; //M ou F

    //gets e sets
    }

}


