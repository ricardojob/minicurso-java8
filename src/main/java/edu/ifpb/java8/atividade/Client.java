/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.java8.atividade;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author Ricardo Job
 */
public class Client {

    public static void main(String[] args) {
        System.out.println(Calculadora.somar(2d, 3d));
        System.out.println(Calculadora.somar(2d, 8d));
        System.out.println(Calculadora.subtrair(2d, 8d));
        System.out.println(Calculadora.subtrair(20d, 8d));

        double valor = Calculadora.calcular(2d, 3d, new OperatorDouble() {

            @Override
            public Double aplicar(Double a, Double b) {
                return a + b;
            }
        });
        System.out.println(valor);
        OperatorDouble c = (a, b) -> a > b ? a : b;
        System.out.println(Calculadora.calcular(2d, 3d, c));
        System.out.println(Calculadora.calcular(2d, 30d, c));

        Runnable r = new Runnable() {

            @Override
            public void run() {
                System.out.println("Olaa, tudo bem?!");
            }
        };

        r.run();

        Runnable retorno = () -> System.out.println("Olaa, tudo bem?!");
        retorno.run();

        Pessoa p1 = new Pessoa("Chaves", 8);
        Pessoa p2 = new Pessoa("Kiko", 9);
        Pessoa p3 = new Pessoa("Chiquinha", 10);

        List<Pessoa> pessoas = Arrays.asList(p1, p2, p3);

//        Comparator<Pessoa> comp = (pessoa1, pessoa2) -> {
//           
//            return Integer.compare(pessoa1.getNome().length(),
//                    pessoa2.getNome().length());
//        };
        //Collections.sort(pessoas, comp);
        // Collections.sort(pessoas, (pessoa1,pessoa2)-> pessoa1.getIdade());
//        pessoas.sort(comp);
        pessoas.sort(
                (pessoa1, pessoa2)
                -> Integer.compare(pessoa1.getNome().length(),
                        pessoa2.getNome().length()));

        long valorRetorno = pessoas.stream().
                filter(p -> p.getIdade() > 8).
                filter(a -> a.getNome().endsWith("a")).
                count();

        System.out.println("Valor: " + valorRetorno);

        for (Pessoa pessoa : pessoas) {
            System.out.println(pessoa);
        }

        List<Pessoa> novalor = pessoas.stream().
                filter(p -> p.getIdade() > 8).
                collect(Collectors.toList());
        System.out.println("Após a filtragem");

        novalor.forEach(novo -> System.out.println(novo));

        List<String> t = pessoas.stream().
                filter(p -> p.getIdade() > 8).
                map(p -> p.getNome().toUpperCase()).
                collect(Collectors.toList());

        t.forEach(a -> System.out.println(a));

    }
}
