/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.java8.atividade;

import java.time.LocalDate;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Ricardo Job
 */
public class Atividade1 {
    public static void main(String[] args) {
        //21-10-2014
        Calendar c = Calendar.getInstance();
        c.set(2014, 9, 21);
        Date data = c.getTime();
        
        System.out.println(c);
        
        LocalDate data1 = Year.of(2014).atMonth(10).atDay(21);
        
        System.out.println(data);
        System.out.println(data1);
        
        c.set(Calendar.YEAR, 2013);
        
        System.out.println(data);
        System.out.println(c);
    }
}
