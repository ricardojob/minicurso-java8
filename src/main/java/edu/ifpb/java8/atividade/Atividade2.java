package edu.ifpb.java8.atividade;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Ricardo Job
 */
public class Atividade2 {
    public static void main(String[] args) {
        //25-12-2014 7:30
        LocalDateTime data = LocalDateTime.of(2014, 12, 25, 7, 30);
        System.out.println(data);
        
    }
}
