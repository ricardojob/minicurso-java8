package edu.ifpb.java8.atividade;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;

/**
 *
 * @author Ricardo Job
 */
public class Atividade3 {
    public static void main(String[] args) {
        //25-12-2014 7:30
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        formatter = formatter.withResolverStyle(ResolverStyle.STRICT);
        
        
        String s = "31-09-2012";
        LocalDate a = LocalDate.parse(s, formatter);
        System.out.println(a);
        
    }
}
