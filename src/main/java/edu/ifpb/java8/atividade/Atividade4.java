package edu.ifpb.java8.atividade;

import java.time.LocalDate;
import java.time.Period;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author Ricardo Job
 */
public class Atividade4 {
    public static void main(String[] args) {
        //26-04-1995 7:30
        LocalDate data=Year.of(1995).atMonth(4).atDay(26);
        LocalDate dataHoje=LocalDate.now();
        Period periodo=Period.between(data, dataHoje);
        long dias=periodo.getDays();
        System.out.println(dias);
        dias=data.until(dataHoje,ChronoUnit.YEARS);
          System.out.println(dias);
    }
}
