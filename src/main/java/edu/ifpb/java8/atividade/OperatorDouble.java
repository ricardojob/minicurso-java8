/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.java8.atividade;

/**
 *
 * @author Ricardo Job
 */

@FunctionalInterface
public interface OperatorDouble {
    public Double aplicar(Double a, Double b);
}
