package edu.ifpb.java8;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.MonthDay;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Locale;

/**
 * @since 27/11/2014
 * @author Ricardo Job
 */
public class NovidadesDateTime {

    public static void main(String args[]) {
        LocalDate atual = LocalDate.now();
        System.out.println("Agora: " + atual.toString());

        NovidadesDateTime classe = new NovidadesDateTime();
        classe.exemploEnums();
        classe.exemploLocalDate();
        classe.exemploYearMonth();
        classe.exemploMonthDay();
        classe.exemploLocalDateTime();
        classe.exemploDateTimeParse();
        classe.exemploDateTimeFormat();
    }

    private void exemploEnums() {
        // Enums: Dia da Semana e Mês
        DayOfWeek dataSomada = DayOfWeek.MONDAY.plus(4);
        System.out.printf("%s%n", dataSomada);
        Locale locale = Locale.getDefault();
        System.out.println(dataSomada.getDisplayName(TextStyle.FULL, locale));
        System.out.println(dataSomada.getDisplayName(TextStyle.NARROW, locale));
        System.out.println(dataSomada.getDisplayName(TextStyle.SHORT, locale));
        System.out.println();
        System.out.printf("%d%n", Month.MARCH.maxLength());
        System.out.println();
        Month mes = Month.AUGUST;
        System.out.println(mes.getDisplayName(TextStyle.FULL, locale));
        System.out.println(mes.getDisplayName(TextStyle.NARROW, locale));
        System.out.println(mes.getDisplayName(TextStyle.SHORT, locale));
    }

    private void exemploLocalDate() {
        LocalDate aberturaCopa = LocalDate.of(2014, Month.JUNE, 12);
        LocalDate proximoSabado = aberturaCopa.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
        System.out.println(proximoSabado);
    }

    private void exemploYearMonth() {
        YearMonth data = YearMonth.now();
        System.out.printf("%s: %d%n", data, data.lengthOfMonth());
        YearMonth data1 = YearMonth.of(2010, Month.FEBRUARY);
        System.out.printf("%s: %d%n", data1, data1.lengthOfMonth());
        YearMonth data2 = YearMonth.of(2012, Month.FEBRUARY);
        System.out.printf("%s: %d%n", data2, data2.lengthOfMonth());
    }

    private void exemploMonthDay() {
        MonthDay data = MonthDay.of(Month.FEBRUARY, 29);
        MonthDay data2 = MonthDay.now();
        System.out.println(data2);
        System.out.println(data + " " + data.isValidYear(2012));
    }

    private void exemploLocalDateTime() {
        System.out.printf("Agora: %s%n", LocalDateTime.now());
        System.out.printf("15 de Abril de 1994 11:30am: %s%n", LocalDateTime.of(1994, Month.APRIL, 15, 11, 30));
        System.out.printf("+ 6 meses: %s%n", LocalDateTime.now().plusMonths(6));
        System.out.printf("- 6 meses: %s%n", LocalDateTime.now().minusMonths(3));
    }

    private void exemploDateTimeParse() {
        String entrada = "20111203"; // BASIC_ISO_DATE
        LocalDate data = LocalDate.parse(entrada, DateTimeFormatter.BASIC_ISO_DATE);
        System.out.println(data);
        String entrada2 = "Jan 28 2003";
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("MMM d yyyy", Locale.getDefault());
        LocalDate data2 = LocalDate.parse(entrada2, formatador);
        System.out.printf("%s%n", data2);
    }

    private void exemploDateTimeFormat() {
        LocalDateTime ldt = LocalDateTime.now();
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("MMM d yyyy  hh:mm a");
        String saida = ldt.format(formatador);
        System.out.println(saida);
        System.out.printf("LEAVING:  %s (%s)%n", saida, ldt);
    }

    public void a() {
        //criar datas
        LocalDate dataAtual = LocalDate.now(); //data atual
        LocalDate data1 = LocalDate.of(2014, 3, 22); //22-mar-2014
        LocalDate data2 = LocalDate.of(2014, Month.MARCH, 22); //22-mar-2014
        LocalDate data3 = Year.of(2010).atMonth(12).atDay(24); //24-dez-2010
        //criar objeto com data e hora
        LocalDateTime dateTimeAtual = LocalDateTime.now();
        LocalDateTime dateTime1 = LocalDateTime.of(2013, Month.MARCH, 21, 21, 10, 1); //21-mar-2013 as 21h10m1s 
    }

    public void s() {

        //criação de um DateTimeFormatter com o padrao dd/MM/yyyy (dia/mes/ano)
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        //criar um objeto LocalDatel a partir da String "15/11/2013"
        String s = "15/11/2013";
        LocalDate data = LocalDate.parse(s, formatter);
        System.out.println(data);

        //pegar a data atual e transformar em String usando o mesmo formatter
        LocalDate dataAtual = LocalDate.now();
        String dataAtualString = dataAtual.format(formatter);
        System.out.println(dataAtualString);

        //Criar um fomatter do tipo "STRICT"
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        formatter = formatador.withResolverStyle(ResolverStyle.STRICT);

        String dia = "31/02/2013"; //31 de fevereiro
        LocalDate dataRetorno = LocalDate.parse(dia, formatador); //exception!

        LocalDate dataInicial = LocalDate.of(2013, Month.NOVEMBER, 15); //data inicial: 15-nov-2013
        LocalDate dataFinal = LocalDate.of(2013, Month.DECEMBER, 31); //data final: 31-dez-2013

        //Recuperar o periodo completo entre as duas datas
        Period periodo = Period.between(dataInicial, dataFinal);
        int anos = periodo.getYears();
        int meses = periodo.getMonths();
        int dias = periodo.getDays();

        String retorno = String.format("Faltam %d ano(s), %d mes(es) e %d dia(s)!", 
                anos, meses, dias);
        System.out.println(retorno); // -> Faltam 0 ano(s), 1 mes(es) e 16 dia(s)!

        //diferença somente em dias
        long numeroDias = dataInicial.until(dataFinal, ChronoUnit.DAYS);
        System.out.println(numeroDias + " dias!"); // -> 46 dias!

    }
}
